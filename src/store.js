import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import HomeDuck from './ducks/Home';

const { reducers: Home } = HomeDuck;

const mainReducer = combineReducers({ Home });

const middleware = [];

if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger'); // eslint-disable-line global-require

  middleware.push(logger);
}
middleware.push(reduxThunk);

export const store = createStore(
  mainReducer,
  {},
  compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : (f) => f,
  ),
);
