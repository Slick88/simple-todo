import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Home from '../pages/Home';

// Add ducks for containers
import homeDucks from '../ducks/Home';

const { addTodoValue, deleteTodo, completeTodo } = homeDucks.operations;

const mapStateToProps = (state) => ({
  todoList: state.Home.todoList || [],
});
const mapDispatchToProps = {
  addTodoValue,
  deleteTodo,
  completeTodo,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Home),
);
