import React from 'react';
import { mount } from 'enzyme';
import Home from './Home';
import { todoStatus } from '../../constants/TodoList';

let HomeWrapper;
let addTodoValueMockFn;
let deleteTodoMockFn;
let completeTodoMockFn;

const sampleTodoList = [{ value: 'test todo', status: todoStatus.ACTIVE }];

const render = (props = {}) =>
  mount(
    <Home
      todoList={sampleTodoList}
      addTodoValue={addTodoValueMockFn}
      deleteTodo={deleteTodoMockFn}
      completeTodo={completeTodoMockFn}
      {...props}
    />,
  );

describe('Home - render', () => {
  beforeEach(() => {
    addTodoValueMockFn = jest.fn();
    deleteTodoMockFn = jest.fn();
    completeTodoMockFn = jest.fn();
    HomeWrapper = render();
  });
  it('should render all components correctly', () => {
    expect(HomeWrapper).toMatchSnapshot();
    expect(HomeWrapper.find('input').exists()).toEqual(true);
    expect(
      HomeWrapper.find('button')
        .first()
        .text(),
    ).toEqual('Cancel');
    expect(
      HomeWrapper.find('button')
        .at(1)
        .text(),
    ).toEqual('Add Todo');
  });
});

describe('Home - functions', () => {
  beforeEach(() => {
    addTodoValueMockFn = jest.fn();
    deleteTodoMockFn = jest.fn();
    completeTodoMockFn = jest.fn();
    HomeWrapper = render({ todoList: [] });
  });
  it('clearInput should set state correctly', () => {
    const testInput = 'Test';
    HomeWrapper.setState({ todoValue: testInput });
    expect(HomeWrapper.state('todoValue')).toEqual(testInput);
    HomeWrapper.instance().clearInput();
    expect(HomeWrapper.state('todoValue')).toEqual('');
  });
  it('handleAddTodo should add to List correctly', () => {
    expect(HomeWrapper.prop('todoList')).toEqual([]);
    HomeWrapper.setState({ todoValue: sampleTodoList[0].value });
    HomeWrapper.instance().handleAddTodo();
    expect(addTodoValueMockFn).toBeCalledTimes(1);
    expect(addTodoValueMockFn).toBeCalledWith(sampleTodoList[0]);
  });
  it('handleInputChange should should set state correctly', () => {
    expect(HomeWrapper.state('todoValue')).toEqual('');
    HomeWrapper.instance().handleInputChange(sampleTodoList[0].value);
    expect(HomeWrapper.state('todoValue')).toEqual(sampleTodoList[0].value);
  });
  it('handleKeyPress should should set state correctly', () => {
    expect(HomeWrapper.prop('todoList')).toEqual([]);
    HomeWrapper.setState({ todoValue: sampleTodoList[0].value });
    HomeWrapper.find('input').simulate('keypress', { key: 'Enter' });
    expect(addTodoValueMockFn).toBeCalledTimes(1);
    expect(addTodoValueMockFn).toBeCalledWith(sampleTodoList[0]);
  });
});
