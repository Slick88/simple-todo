export const todoStatus = {
  ACTIVE: 'ACTIVE',
  COMPLETED: 'COMPLETED',
};

export const todoFilter = {
  ALL: 'ALL',
  ...todoStatus,
};

export default {
  todoStatus,
  todoFilter,
};
