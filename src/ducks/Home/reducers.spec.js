import reducer from './reducers';
import types from './types';

it('should load initial state', () => {
  const initialState = {
    suggestedValue: [],
  };
  const action = {
    type: types.INITIATE_DETAILS,
  };
  const result = reducer(initialState, action);
  expect(result.suggestedValue).toEqual([]);
});
