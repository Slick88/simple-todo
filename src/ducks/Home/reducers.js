import types from './types';
/*eslint-disable*/

const initialState = {
  todoList: [],
};

const {
  INITIATE_HOME,
  STORE_TODO_LIST,
  RESET_TODO_LIST,
  ADD_TO_TODO_LIST,
  DELETE_FROM_TODO_LIST,
  COMPLETE_TODO,
} = types;

const movie = (state = initialState, action) => {
  switch (action.type) {
    case INITIATE_HOME: {
      return initialState;
    }
    case STORE_TODO_LIST: {
      return { todoList: action.todoList };
    }
    case RESET_TODO_LIST: {
      return { todoList: [] };
    }
    case ADD_TO_TODO_LIST: {
      return { ...state, todoList: [...state.todoList, action.todo] };
    }
    case DELETE_FROM_TODO_LIST: {
      return {
        ...state,
        todoList: state.todoList.splice(action.index, 1),
      };
    }
    case COMPLETE_TODO: {
      return {
        ...state,
        todoList: [
          ...state.todoList.splice(0, action.index),
          action.updatedTodo,
          ...state.todoList.splice(action.index + 1),
        ],
      };
    }
    default:
      return state;
  }
};

export default movie;
