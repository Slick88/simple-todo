/* eslint-disable*/
import actions from './actions';
import { todoStatus } from '../../constants/TodoList';

const getSuggestedValue = (value) => (dispatch) => {
  // we can call a back end at this point to get persistent backend data
  const suggestedValue = [];
  dispatch(actions.storeSuggestedValue(suggestedValue));
};

const resetSuggestedValue = () => (dispatch) => {
  dispatch(actions.resetSuggestedValue());
};

const addTodoValue = (value) => (dispatch) => {
  dispatch(actions.addTodoValue(value));
};

const deleteTodo = (value) => (dispatch) => {
  dispatch(actions.deleteTodo(value));
};

const completeTodo = (value) => (dispatch, getState) => {
  const updateTodo = getState().Home.todoList[value];
  updateTodo.status = todoStatus.COMPLETED;
  dispatch(actions.completeTodo(updateTodo, value));
};

export default {
  getSuggestedValue,
  resetSuggestedValue,
  addTodoValue,
  deleteTodo,
  completeTodo,
};
