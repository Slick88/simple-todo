import types from './types';

const storeSuggestedValue = (todoList) => ({
  type: types.STORE_TODO_LIST,
  todoList,
});

const resetSuggestedValue = () => ({
  type: types.RESET_TODO_LIST,
});

const addTodoValue = (todo) => ({
  type: types.ADD_TO_TODO_LIST,
  todo,
});

const deleteTodo = (index) => ({
  type: types.DELETE_FROM_TODO_LIST,
  index,
});

const completeTodo = (updatedTodo, index) => ({
  type: types.COMPLETE_TODO,
  updatedTodo,
  index,
});

export default {
  storeSuggestedValue,
  resetSuggestedValue,
  addTodoValue,
  deleteTodo,
  completeTodo,
};
