/*eslint-disable*/
import React, { Component } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';
import styles from './Home.module.css';
import { todoStatus } from '../../constants/TodoList';
import TodoList from './TodoList';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoValue: '',
      filter: todoStatus.ACTIVE,
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.todoList !== nextProps.todoList) {
      console.log('This is this.props.todoList', this.props.todoList);
      console.log('This is nextProps.todoList', nextProps.todoList);
    }
  }

  clearInput = () => {
    this.setState({
      todoValue: '',
    });
  };

  handleAddTodo = () => {
    const { addTodoValue } = this.props;
    addTodoValue({
      value: this.state.todoValue,
      status: todoStatus.ACTIVE,
    });
    this.clearInput();
  };

  handleInputChange = (value) => {
    this.setState({
      todoValue: value,
    });
  };

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.handleAddTodo();
    }
  };

  render() {
    const placeholder = 'Enter a value';
    const { todoValue } = this.state;
    const { deleteTodo, completeTodo } = this.props;

    return (
      <Container className={styles.pageOverview}>
        <Row>
          <h1 className={styles.title}>My Todo List</h1>
        </Row>
        <Row>
          <Col xl="8" lg="8" md="6" xs={6}>
            <input
              className={styles.specialInput}
              type="text"
              placeholder={placeholder}
              value={todoValue}
              onChange={(e) => this.handleInputChange(e.target.value)}
              onKeyPress={(e) => this.handleKeyPress(e)}
            />
          </Col>
        </Row>
        <Row>
          <Col xl="1" lg="2" md="2" xs={6}>
            <Button variant="outline-danger" onClick={this.clearInput}>
              Cancel
            </Button>
          </Col>
          <Col xl="4" lg="4" md="4" xs={6}>
            <Button variant="outline-primary" onClick={this.handleAddTodo}>
              Add Todo
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <TodoList
              todoList={this.props.todoList}
              handleDelete={deleteTodo}
              handleComplete={completeTodo}
            />
          </Col>
          {/*add in the render</Row>*/}
        </Row>
      </Container>
    );
  }
}

Home.propTypes = {
  addTodoValue: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  completeTodo: PropTypes.func.isRequired,
  todoList: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      status: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default Home;
