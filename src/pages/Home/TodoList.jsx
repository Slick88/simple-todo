/*eslint-disable*/
import React, { Component } from 'react';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { Breadcrumb, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMinus, faCheck } from '@fortawesome/free-solid-svg-icons';

import { todoFilter, todoStatus } from '../../constants/TodoList';
import styles from './TodoList.module.css';

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: todoFilter.ALL,
      list: this.props.todoList,
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.todoList !== nextProps.todoList) {
      this.setState({
        list: nextProps.todoList,
      });
    }
  }

  renderFilter = () => {
    const crumbs = Object.keys(todoFilter).map((filter) => {
      return (
        <Breadcrumb.Item
          key={filter}
          active={filter === this.state.filter}
          onClick={this.setFilter}
        >
          {filter}
        </Breadcrumb.Item>
      );
    });

    return <Breadcrumb>{crumbs}</Breadcrumb>;
  };

  setFilter = (e) => {
    const currentFilter = e.target.innerText;
    let newList;
    if (currentFilter === todoFilter.ALL) {
      newList = this.props.todoList;
    } else {
      newList = this.props.todoList.filter(
        (todo) => todo.status === currentFilter,
      );
    }

    this.setState({
      filter: currentFilter,
      list: newList,
    });
  };

  rowContentStyle = (item) => {
    return {
      textDecoration:
        item.status === todoStatus.COMPLETED ? 'line-through' : '',
    };
  };

  renderTable = () => {
    const { list } = this.state;
    const { handleDelete, handleComplete } = this.props;
    if (isEmpty(list)) {
      return;
    }
    const calculateView = list.map((item, index) => {
      return (
        <tr key={item.value + index}>
          <td style={this.rowContentStyle(item)}>{index + 1}</td>
          <td colSpan="5" style={this.rowContentStyle(item)}>
            {item.value}
          </td>
          <td>
            {
              <FontAwesomeIcon
                className={styles.todoIcons}
                icon={faMinus}
                onClick={() => handleDelete(index)}
              />
            }
            {item.status !== todoStatus.COMPLETED && (
              <FontAwesomeIcon
                className={styles.todoIcons}
                icon={faCheck}
                onClick={() => handleComplete(index)}
              />
            )}
          </td>
        </tr>
      );
    });
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th colSpan="5">Todo</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{calculateView}</tbody>
      </Table>
    );
  };

  render() {
    return (
      <div>
        {this.renderFilter()}
        {this.renderTable()}
      </div>
    );
  }
}

TodoList.propTypes = {
  handleDelete: PropTypes.func.isRequired,
  handleComplete: PropTypes.func.isRequired,
  todoList: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      status: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default TodoList;
