import { cloneDeep } from 'lodash';
import actions from './actions';
import types from './types';
import { todoStatus } from '../../constants/TodoList';

const sampleTodoList = [{ value: 'test', status: todoStatus.ACTIVE }];

describe('actions - trigger storeSuggestedValue', () => {
  it('should trigger storeSuggestedValue', () => {
    const expectedAction = {
      type: types.STORE_TODO_LIST,
      todoList: sampleTodoList,
    };
    expect(actions.storeSuggestedValue(sampleTodoList)).toEqual(expectedAction);
  });
});

describe('actions - trigger resetSuggestedValue', () => {
  it('should trigger resetSuggestedValue', () => {
    const expectedAction = {
      type: types.RESET_TODO_LIST,
    };
    expect(actions.resetSuggestedValue()).toEqual(expectedAction);
  });
});

describe('actions - trigger addTodoValue', () => {
  it('should trigger addTodoValue', () => {
    const expectedAction = {
      type: types.ADD_TO_TODO_LIST,
      todo: sampleTodoList[0],
    };
    expect(actions.addTodoValue(sampleTodoList[0])).toEqual(expectedAction);
  });
});

describe('actions - trigger deleteTodo', () => {
  it('should trigger deleteTodo', () => {
    const expectedAction = {
      type: types.DELETE_FROM_TODO_LIST,
      index: 0,
    };
    expect(actions.deleteTodo(0)).toEqual(expectedAction);
  });
});

describe('actions - trigger completeTodo', () => {
  it('should trigger completeTodo', () => {
    const updatedTodo = cloneDeep(sampleTodoList);
    updatedTodo.status = todoStatus.COMPLETED;
    const expectedAction = {
      type: types.COMPLETE_TODO,
      updatedTodo,
      index: 0,
    };
    expect(actions.completeTodo(updatedTodo, 0)).toEqual(expectedAction);
  });
});
